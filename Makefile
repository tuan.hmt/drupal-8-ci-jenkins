# Add utility functions and scripts to the container
include scripts/makefile/*.mk


# https://stackoverflow.com/a/6273809/1826109
%:
	@:

# Prepare enviroment variables from defaults
$(shell false | cp -i \.env.default \.env 2>/dev/null)
$(shell false | cp -i \.\/docker\/docker-compose\.override\.yml\.default \.\/docker\/docker-compose\.dev\.yml 2>/dev/null)
include .env

# Set COMPOSE FILE
ifeq ($(PROJECT_ENV), dev)
	export COMPOSE_FILE := ./docker/docker-compose.dev.yml
endif

# Get user/group id to manage permissions between host and containers
LOCAL_UID := $(shell id -u)
LOCAL_GID := $(shell id -g)

# Evaluate recursively
CUID ?= $(LOCAL_UID)
CGID ?= $(LOCAL_GID)

# Define network name.
COMPOSE_NET_NAME := $(COMPOSE_PROJECT_NAME)_front

# Execute php container as regular user
php = docker-compose exec -T --user $(CUID):$(CGID) php ${1}
# Execute php container as root user
php-0 = docker-compose exec -T php ${1}

## Full site install from the scratch
all: | provision build update dev

## Provision enviroment
provision:
# Check if enviroment variables has been defined
ifeq ($(strip $(COMPOSE_PROJECT_NAME)),projectname)
	$(info Project name can not be default, please enter project name.)
	$(eval COMPOSE_PROJECT_NAME = $(strip $(shell read -p "Project name: " REPLY;echo -n $$REPLY)))
	$(shell sed -i -e '/COMPOSE_PROJECT_NAME=/ s/=.*/=$(COMPOSE_PROJECT_NAME)/' .env)
	$(info Please review your project settings and run `make all` again.)
	exit 1
endif
	make -s down
	@echo "Updating containers..."
	docker-compose pull
	@echo "Build and run containers..."
	docker-compose up -d --remove-orphans

build:
ifeq ($(PROJECT_ENV), dev)
	@echo "PROJECT_ENV=$(PROJECT_ENV)"
	@echo "Installing composer dependencies, including dev ones"
	$(call php-0, chmod +w web/sites/default)
	@$(call php, composer install --prefer-dist -o)
else
	@echo "INSTALL_DEV_DEPENDENCIES set to FALSE or missing from .env"
	@echo "Installing composer dependencies, without dev ones"
	$(call php-0, chmod +w web/sites/default)
	@$(call php, composer install --prefer-dist -o --no-dev)
endif

front:
	@echo "Building front"
	$(call php, cd ./web/themes/custom/bycnit_sass && npm install && ./node_modules/.bin/gulp)

## Install drupal
si:
	@echo "Installing from: $(PROJECT_INSTALL)"
	$(call php, ./vendor/bin/drush si $(PROFILE_NAME) --db-url=$(DB_URL) --account-name=$(ADMIN_NAME) --account-mail=$(ADMIN_MAIL) --account-pass=$(ADMIN_PW) -y --site-name="$(SITE_NAME)" --site-mail="$(SITE_MAIL)" -y)

## Update Drupal
update:
	@echo "Updating from: $(PROJECT_INSTALL)"
	@$(call php, ./vendor/bin/drush updb -y)
	@$(call php, ./vendor/bin/drush edel shortcut -y)
	@$(call php, ./vendor/bin/drush config-set 'system.site' uuid $(SITE_UUID) -y)
	@$(call php, ./vendor/bin/drush cim -y)
	@$(call php, ./vendor/bin/drush cr)

## Run shell in PHP container as regular user
exec:
	docker-compose exec --user $(CUID):$(CGID) php sh

## Run shell in PHP container as root
exec0:
	docker-compose exec php ash

down:
	@echo "Removing network & containers for $(COMPOSE_PROJECT_NAME)"
	@docker-compose down -v --remove-orphans --rmi local

## Enable development mode and disable caching
dev:
	@echo "Dev tasks..."
	$(call php-0, chmod +w web/sites/default)
	@echo "Enabling devel module."
	@$(call php, ./vendor/bin/drush -y -q en devel devel_generate kint)
	@echo "Disabling caches."
	@$(call php, ./vendor/bin/drupal -y site:mode dev)
	@$(call php, ./vendor/bin/drush -y -q pm-uninstall dynamic_page_cache page_cache)
	@$(call php, ./vendor/bin/drush cr)

## Run drush command in PHP container. To pass arguments use double dash: "make drush dl devel -- -y"
drush:
	$(call php, $(filter-out "$@",./vendor/bin/$(MAKECMDGOALS)))
	$(info "To pass arguments use double dash: "make drush dl devel -- -y"")

composer:
	$(call php, $(filter-out "$@",$(MAKECMDGOALS)))
	$(info "To pass arguments use double dash: "make composer require ..."")
