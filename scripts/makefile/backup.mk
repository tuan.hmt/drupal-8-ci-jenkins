## Make backup from current state
mysql_dump_name = $(COMPOSE_PROJECT_NAME).sql
files_dir = scripts
datestamp=$(shell echo `date +'%Y-%m-%d'`)
backup_name = $(COMPOSE_PROJECT_NAME)-$(datestamp).tar.gz

backup:
	rm -f $(backup_name)
	$(call php, drush sql-dump --database=default --result-file=../$(mysql_dump_name))