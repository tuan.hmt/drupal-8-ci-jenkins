#!/bin/bash

# Provision - docker-compose.yml
docker-compose down
# Remove code volume
if docker volume ls | grep -q $CODE_IMAGE_NAME; then
  docker volume rm $CODE_IMAGE_NAME;
fi

# Update images
docker-compose pull
# Start all containers
docker-compose up -d

# Docker compose exec function
RUN () {
  docker-compose exec -T php sh -c "$1"
}

# If Drupal has not installed yet.

# 1. Load env variable
if [ -f .env ]
then
  export $(cat .env | sed 's/#.*//g' | xargs)
fi

# 2. Install Drupal
RUN "if ./vendor/bin/drush status bootstrap | grep -q 'Successful'; then
  echo 'Drupal has been installed.';
else
  ./vendor/bin/drush si standard --db-url=$DB_URL --account-name=$ADMIN_NAME --account-mail=$ADMIN_MAIL --account-pass=$ADMIN_PW -y && ./vendor/bin/drush edel shortcut -y;
fi"

# 1. Set site UUID
RUN "./vendor/bin/drush config-set 'system.site' uuid $SITE_UUID"
# 2. Update Drupal DB
RUN "./vendor/bin/drush updb -y"
# 3. Import Drupal Configs
RUN "./vendor/bin/drush cim -y"

# This will be removed when the php-fpm permission issue has been resolved
RUN "./vendor/bin/drush -y config-set system.performance css.preprocess 0"
RUN "./vendor/bin/drush -y config-set system.performance js.preprocess 0"

RUN "chown -R www-data:www-data /var/www/html"

RUN "./vendor/bin/drush cache:rebuild"